#!/usr/local/bin/php -q
<?php
    require '../config/config.php';
    require '../config/conn.php';

error_reporting(E_ALL);

/* Allow the script to hang around waiting for connections. */
set_time_limit(0);

/* Turn on implicit output flushing so we see what we're getting
 * as it comes in. */
ob_implicit_flush();

$address = '192.168.1.14';
// $address = '81.169.145.81';
$port = 10001;
$route_id;
$distance;
$orientation;
echo "start Server";

if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}

if (socket_bind($sock, $address, $port) === false) {
    echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}

if (socket_listen($sock, 5) === false) {
    echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}

do {
    if (($msgsock = socket_accept($sock)) === false) {
        echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
        break;
    }

    do {
        if (false === ($buf = socket_read($msgsock, 2048, PHP_BINARY_READ))) {
            echo "socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)) . "\n";
            break 2;
        }
        if (!$buf = trim($buf)) {
            continue;
        }

        $pieces = explode("_", $buf);
        if (count($pieces) === 1){
            $route_id = $pieces[0];
            echo "Route_id is:". $route_id. "\n";
        } else {
            $distance = $pieces[0];
            $orientation = $pieces[1];
            echo "Distance is ". $distance . "en Orientation is:" . $orientation . "\n";
            $sql = "INSERT INTO routes (routeId, orientation, speed) VALUES ('".$route_id."', '".$orientation."', '".$distance."')";
            $mysqli->query($sql);
        };

        $talkback = "PHP: You said '".$buf."'\n";
        socket_write($msgsock, $talkback, strlen($talkback));
        // echo $buf. "\n";
    } while (true);
    socket_close($msgsock);
} while (true);

socket_close($sock);
?>