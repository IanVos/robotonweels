package com.example.myapplication;

import android.media.Image;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import static java.lang.System.out;

public class InterfaceHandler {

    private View rootView;
    private TCPSocket socketClient;
    private byte roverMovement; // Movement byte, sent to rover and writen in motor controler reg

    /***
     * The bit position for the specified movement (Defined in motor controller)
     */
    static class MovementBitPositions {
        static byte forward = 1 << 0;       // 0000 0001
        static byte backward = 1 << 1;      // 0000 0010
        static byte left = 1 << 2;          // 0000 0100
        static byte right = 1 << 3;         // 0000 1000
    }

    static class roverDirections {
        static int stop = -1;
        static int forward = 0;
        static int backward = 1;
        static int left = 2;
        static int right = 3;
    }

    public InterfaceHandler(View rootV, TCPSocket clientSocket)
    {
        rootView = rootV;
        socketClient = clientSocket;
        setDefaultLayout();
    }

    private void setDefaultLayout()
    {
        rootView.findViewById(R.id.text_compval).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.text_speed).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.face_found_box).setVisibility(View.GONE);
        rootView.findViewById(R.id.warning_box).setVisibility(View.GONE);
        rootView.findViewById(R.id.speaker_button).setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.switch_cammode).setVisibility(View.GONE);
        rootView.findViewById(R.id.switch_distance).setVisibility(View.GONE);
        rootView.findViewById(R.id.switch_speed).setVisibility(View.GONE);
        rootView.findViewById(R.id.switch_compass).setVisibility(View.GONE);
    }


    public void onClick(View view)
    {
        // Get reference to all UI objects.
        Button button_left = rootView.findViewById(R.id.button_left);
        Button button_right = rootView.findViewById(R.id.button_right);
        Button button_up = rootView.findViewById(R.id.button_up);
        Button button_down = rootView.findViewById(R.id.button_down);
        Button button_options = rootView.findViewById(R.id.button_options);
        Button button_speaker = rootView.findViewById(R.id.speaker_button);
        Switch speed_switch = rootView.findViewById(R.id.switch_speed);
        Switch distance_switch = rootView.findViewById(R.id.switch_distance);
        TextView speed_text = rootView.findViewById(R.id.text_speed);
        View warning_box = rootView.findViewById(R.id.warning_box);
        Switch switch_cammode = rootView.findViewById(R.id.switch_cammode);
        Switch switch_compass = rootView.findViewById(R.id.switch_compass);
        TextView compValue_text = rootView.findViewById(R.id.text_compval);
        TextView distance_Text = rootView.findViewById(R.id.distanceValue);
        ImageView distance_Image = rootView.findViewById(R.id.image_distance);
        // Event listener for showing/hiding the compass value when clicked.
        switch_compass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switch_compass.isChecked()  == true){
                    compValue_text.setVisibility(View.VISIBLE);
                    socketClient.sendToServer("31");
                }
                else {
                    compValue_text.setVisibility(View.GONE);
                    socketClient.sendToServer("30");
                }

            }
        });

        // Event listener for showing/hiding the speed value when clicked.
        speed_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(speed_switch.isChecked()  == true){
                    speed_text.setVisibility(View.VISIBLE);
                    socketClient.sendToServer("11");
                }
                else {
                    speed_text.setVisibility(View.GONE);
                    socketClient.sendToServer("10");
                }

            }
        });

        // Event listener for swapping to infrared/regular when clicked.
        switch_cammode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switch_cammode.isChecked()  == true){
                    // Send message to rover to turn on infrared.
                    socketClient.sendToServer("41");
                }
                else {
                    // Send message to rover to turn off infrared.
                    socketClient.sendToServer("40");
                }

            }
        });

        // Event listener for toggling the distance value on screen.
        distance_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(distance_switch.isChecked()  == true){
                    distance_Image.setVisibility(View.VISIBLE);
                    distance_Text.setVisibility(View.VISIBLE);
                    socketClient.sendToServer("21");
                }
                else {
                    distance_Image.setVisibility(View.GONE);
                    distance_Text.setVisibility(View.GONE);
                    socketClient.sendToServer("20");
                }

            }
        });

        // Event listener for showing/hiding the menu options.
        button_options.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent m) {
                        if(m.getActionMasked() == MotionEvent.ACTION_DOWN)
                        {
                            if(speed_switch.getVisibility() == View.GONE){

                                speed_switch.setVisibility(View.VISIBLE);
                                distance_switch.setVisibility(View.VISIBLE);
                                switch_cammode.setVisibility(View.VISIBLE);
                                switch_compass.setVisibility(View.VISIBLE);
                            }
                            else {

                                speed_switch.setVisibility(View.GONE);
                                distance_switch.setVisibility(View.GONE);
                                switch_cammode.setVisibility(View.GONE);
                                switch_compass.setVisibility(View.GONE);
                            }
                        }
                        return true;
                    }
                }
        );

        button_speaker.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent m) {
                        if (m.getActionMasked() == MotionEvent.ACTION_DOWN){
                            socketClient.sendToServer("5"); // Send a message to the server
                        }
                        return true;
                    }
                }
        );


        /**
         * Left button
         */
        button_left.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent m) {
                        if (m.getActionMasked() == MotionEvent.ACTION_UP) {
                            out.printf("SEND: Stop, %d", InterfaceHandler.roverDirections.stop);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.stop); // Send a message to the server
                        }
                        else if (m.getActionMasked() == MotionEvent.ACTION_DOWN){
                            int tmp = (~InterfaceHandler.MovementBitPositions.left & roverMovement) | InterfaceHandler.MovementBitPositions.left;


                            out.printf("SEND: Left, %d", InterfaceHandler.roverDirections.left);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.left); // Send a message to the server

                        }
                        return true;
                    }
                }
        );




        /**
         * Right button
         */
        button_right.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent m) {
                        if (m.getActionMasked() == MotionEvent.ACTION_UP) {
                            out.printf("SEND: Stop, %d", InterfaceHandler.roverDirections.stop);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.stop); // Send a message to the server
                        }
                        else if (m.getActionMasked() == MotionEvent.ACTION_DOWN){
                            out.printf("SEND: Right, %d", InterfaceHandler.roverDirections.right);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.right); // Send a message to the server
                        }
                        return true;
                    }
                }
        );

        /**
         * Backward button
         */
        button_down.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent m) {
                        if (m.getActionMasked() == MotionEvent.ACTION_UP) {
                            out.printf("SEND: Stop, %d", InterfaceHandler.roverDirections.stop);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.stop); // Send a message to the server
                        }
                        else if (m.getActionMasked() == MotionEvent.ACTION_DOWN){
                            out.printf("SEND: Backward, %d", InterfaceHandler.roverDirections.backward);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.backward); // Send a message to the server
                        }
                        return true;
                    }
                }
        );

        /**
         * Forward button
         */
        button_up.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent m) {
                        if (m.getActionMasked() == MotionEvent.ACTION_UP) {
                            out.printf("SEND: Stop, %d", InterfaceHandler.roverDirections.stop);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.stop); // Send a message to the server
                        }
                        else if (m.getActionMasked() == MotionEvent.ACTION_DOWN){
                            out.printf("SEND: Forward, %d", InterfaceHandler.roverDirections.forward);
                            socketClient.sendToServer(InterfaceHandler.roverDirections.forward); // Send a message to the server
                        }
                        return true;
                    }
                }
        );
    }
}
