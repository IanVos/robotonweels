/***
 * @Author Collin Loot & Ian Vos
 * Socket client to connect to an TCP socket server
 */

package com.example.myapplication;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.net.Socket;
import java.io.*;
import java.net.UnknownHostException;

public class TCPSocket implements Runnable {
    // Initialise variables
    private Socket connection; // Create an empty socket object
    private int port;
    private String host;

    static private PrintWriter out; // Output stream
    static private BufferedReader in;  // input stream

    private TextView speedValue;
    private TextView compassValue;
    private TextView distanceValue;

    private ConstraintLayout personFoundWarning;

    private Activity mActivity;


    /***
     * Set the socket variables
     * @param port the port the socket has to connect to
     * @param ipAdress The host the socket has to connect to
     */
    public TCPSocket(int port, String ipAdress, TextView speedVal, TextView compassVal, TextView distanceVal, ConstraintLayout personWarning, Activity activity){
        this.port = port;
        this.host = ipAdress;

        speedValue = speedVal;
        compassValue = compassVal;
        distanceValue = distanceVal;
        personFoundWarning = personWarning;
        mActivity = activity;
    }

    /***
     * Start the connection with the server
     */
    private void startConnection() {
        try {
            // Create a socket
            connection = new Socket(this.host, this.port);

            // Initialise the input and output streams
            out = new PrintWriter(new OutputStreamWriter(connection.getOutputStream()), true);
            in = new java.io.BufferedReader(new InputStreamReader(connection.getInputStream()));

            // Listen to incoming messages
            while (true) {
                String serverMessage = in.readLine();
                char[] splittedMessage = serverMessage.toCharArray();

                if (serverMessage != null) {
                    String concatMessage = "";
                    char sensorType = ' ';
                    boolean foundActualMessage = false;

                    for (int i = 0; i < splittedMessage.length; i++) {
                        // Check where number starts in array.
                        if (splittedMessage[i] != '\u0000' && foundActualMessage == false) {
                            sensorType = splittedMessage[i];
                            foundActualMessage = true; // If number starts start logging
                            i += 1; // And increase the index of the loop to start at the number

                        }
                        // Check if number is found and array is still in bounds.
                        if (foundActualMessage == true && i < splittedMessage.length) {
                            concatMessage = concatMessage + splittedMessage[i]; // Start logging
                        }
                    }

                    // Place the concatenated message into a var efficiently final.
                    Pair<Character, String> sensorData = new Pair<Character, String>(sensorType, concatMessage);

                    // Update the UI on the thread it's made on to avoid a data race.
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Check what sensor it is.
                            switch(sensorData.first)
                            {
                                case '3': // Compass
                                    compassValue.setText(sensorData.second + "°");
                                    break;
                                case '1': // Speed
                                    speedValue.setText(sensorData.second + "kp/h");
                                    break;
                                case '2': // Distance
                                    distanceValue.setText(sensorData.second + "cm before object.");
                                    if(Integer.valueOf(sensorData.second) < 130)
                                    {
                                        mActivity.findViewById(R.id.warning_box).setVisibility(View.VISIBLE);
                                    }
                                    else
                                    {
                                        mActivity.findViewById(R.id.warning_box).setVisibility(View.GONE);
                                    }
                                    break;
                                case '9':
                                        personFoundWarning.setVisibility(View.VISIBLE);
                                    break;
                                case 0:
                                    personFoundWarning.setVisibility(View.GONE);
                                    break;
                            }
                        }
                    });
                }
            }
        } catch (UnknownHostException ex){
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex){
            System.out.println("Exception: " + ex.getMessage());
        }
    }

    /***
     * Send a message to the server
     * @param msg (String): The message to be sent
     */
    public void sendToServer(String msg){

        new Thread(new SendThread(msg)).start();
    }
    public void sendToServer(int msg){
        sendToServer(Integer.toString(msg));
    }

    /***
     * Close the socket connection
     */
    public void closeSocket(){
        try{
            if(connection != null){
                connection.close();
                System.out.println("Succesfully closed");
            }
            System.out.println("Closed...");
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    /***
     * Send data to the server
     */
    public class SendThread implements Runnable{
        private String msg;
        public SendThread(String msg){
            this.msg = msg;
        }
        @Override
        public void run(){
            System.out.println("Sending message:");
            System.out.println(msg);
            out.write(msg);
            out.flush();
        }
    }

    @Override
    /**
     * Listen to the client and send to all what is received
     */
    public void run() {
        try{
            startConnection();
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            closeSocket();
        }
    }
}
