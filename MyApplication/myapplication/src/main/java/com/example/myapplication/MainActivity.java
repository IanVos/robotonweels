/***
 * @Author: Siem Dicou & Ian Vos
 * The main activity of the android application where the UI is handeled and sub processes are started
 */

package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.webkit.WebResourceError;
import android.widget.Button;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebResourceRequest;
import android.widget.Toast;
import android.annotation.TargetApi;


import static java.lang.System.*;

public class MainActivity extends Activity {
    WebView webView;
    public TCPSocket socketClient;
    public String ip = "192.168.1.2";
    private InterfaceHandler UIHandler;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = findViewById(R.id.video_view);

        // Start the connection with the server
        out.println("Starting connection with server");
        this.socketClient = new TCPSocket(1337, ip, findViewById(R.id.text_speed), findViewById(R.id.text_compval), findViewById(R.id.distanceValue), findViewById(R.id.face_found_box), this); // Create object that holds needed information
        Thread socketClientThread = new Thread(this.socketClient); // Make a new thread for the connections
        socketClientThread.start(); // Start the new thread

        // Initialize interface handler.
        UIHandler = new InterfaceHandler(findViewById(android.R.id.content).getRootView(), socketClient);
        // Run onclick once so that focus is on buttons from the start.
        onClick(new View(this));
        webView.getSettings().setJavaScriptEnabled(true); // enable javascript
        final Activity activity = this;

        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        webView.loadUrl("http://" + ip + ":1731");
        hideSystemUi();
    }

    private void hideSystemUi() {
        webView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    public void onClick (View view) {
        UIHandler.onClick(view);
    }
}




