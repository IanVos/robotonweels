/***
 * @author Jeffrey Faas & Ian Vos
 * Start the program and create the threads
 ***/

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <wiringPi.h>

/* Local header files */
#include "communication/SocketServer.h"
#include "movement/movement.h"
#include "sensors/ultrasonic.h"
#include "sensors/speed.h"
#include "CameraStream/streamControl.h"
#include "speaker/player.h"
#include "faceRecognition/faceRecognition.h"

#include "compass/Mpu9250.h"
#include "compass/RouteMapping.h"

#define DEBUG_MODE 1

using namespace std;

#if DEBUG_MODE == 1
#define OUT(x) cout << "Main: " << x << endl
#else
#define OUT(x)
#endif


/* Define the threads */
pthread_t speed;
pthread_t ultrasonic;
pthread_t speaker;
pthread_t compassThread;

/***
 * Start the server and parse messages to the correct sub proces
 * @param atr(void*): The atributes that are given to the thread
 ***/
void *serverStarter(void *atr){
  wiringPiSetup() ;
  pinMode (0, OUTPUT);
  digitalWrite (0, HIGH);

  /* Create new objects to run the threads in */
  Ultrasonic ultrasonic_sensor;
  Speedsensor speed_sensor;
  SocketServer server;
  Movement movement;
  
  server.startServer();
  bool clientConnected = false;
  while(true){
    OUT("huts");
    if(clientConnected) {
      int msg;
      msg = server.listenToClient();
      OUT(msg);
      switch(msg){
        case -1: 
          movement.stop();
          break;
        case 0: 
          movement.forward();
          break;
        case 1:  
          movement.backward();
          break;
        case 2: 
          movement.left();
          break;
        case 3: 
          movement.right();
          break;
        case 5:
          pthread_create(&speaker, NULL, &Player::start, NULL);
          break;
        case 10:
          pthread_cancel(speed);
          break;
        case 11:
          pthread_create(&speed, NULL, &Speedsensor::speed_helper, &speed_sensor);
          break;
        case 20:
          pthread_cancel(ultrasonic);
          break;
        case 21:
          pthread_create(&ultrasonic, NULL, &Ultrasonic::ultrasonic_helper,&ultrasonic_sensor);
          break;
        case 30:
          pthread_cancel(compassThread);
        case 31:
          pthread_create(&compassThread, NULL, &RouteMapping::compassMapping, NULL);  /* Create the commpas thread */ 
        case 40: /* Enable IR*/
          digitalWrite (0, LOW);
          break;
        case 41: /* Disable IR */
          digitalWrite (0, HIGH);
          break;

        case 999:
          OUT("Main: 999, stopping");
          movement.stop();
          pthread_cancel(compassThread);
          pthread_cancel(speed);
          pthread_cancel(ultrasonic);
          pthread_cancel(speaker);
          server.closeSocket();
          clientConnected = false;
          break;
      }
    } 
    else {
      /* Starting to listen to a new client */
      sleep(1);
      OUT("Main: Starting client job interview...");
      server.acceptClient();
      OUT("Main: A new client has been accepted");
      pthread_create(&speed, NULL, &Speedsensor::speed_helper, &speed_sensor);
      pthread_create(&compassThread, NULL, &RouteMapping::compassMapping, NULL);
      pthread_create(&ultrasonic, NULL, &Ultrasonic::ultrasonic_helper,&ultrasonic_sensor);
      clientConnected = true;
    }
  }
}

/***
 * Start all the threads
 ***/
int main(){
    /* Define the threads */
    pthread_t server;
    pthread_t faceRecognition;

    /* Create new objects to run the threads in */
    Movement moveControls;
    StreamControl streamControl;

    /* init the motor controls */
    moveControls.Motorinit();

    /* Start the stream */
    streamControl.streamStart();

    /* Create new threads */
    pthread_create(&server, NULL, &serverStarter, NULL);        
    pthread_create(&faceRecognition, NULL, &FaceRecognition::startFaceRecognition, NULL);

    /* Join the threads */
    pthread_join(server, NULL);
    pthread_join(speed, NULL);
    pthread_join(ultrasonic, NULL);
    pthread_join(speaker, NULL);
    pthread_join(faceRecognition, NULL);
    pthread_join(compassThread, NULL);
    return 0;
}
