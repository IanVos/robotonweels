/* The default static IP of the rover */
#define OWN_IP "192.168.1.2"

/* The default stream settings */
#define STREAM_PORT "1731"
#define STREAM_IP "http://" OWN_IP ":" STREAM_PORT