/***
* @author Jeffrey Faas
* Header file for the audio player
***/

#ifndef PLAYER_H
#define PLAYER_H

#include <stddef.h>

class Player {
    private:
        static unsigned char *buffer;
        static size_t buffer_size;
        static size_t done;
        static int err;
        static int driver;
        static int channels;
        static int encoding;
        static long rate;
        static bool isPlaying;

    public:
        static void initPlayer();
        static void openFile();
        static void setOutputFormat();
        static void playSound();
        static void *start(void* atr);
};

#endif