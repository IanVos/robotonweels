/***
* @author Jeffrey Faas
* This program is an audio player for mp3 files
***/

#include <ao/ao.h>
#include <mpg123.h>
#include "player.h"

#define BITS 8

mpg123_handle *mh;
ao_device *dev;
ao_sample_format format;

/*Global variables*/
unsigned char * Player::buffer;
size_t Player::buffer_size;
size_t Player::done;
int Player::err;
int Player::driver;
int Player::channels;
int Player::encoding;
long Player::rate;
bool Player::isPlaying = false;

/*
* Initializations for the audio player
*/
void Player::initPlayer(){
    /* initializations */
    ao_initialize();
    driver = ao_default_driver_id();
    mpg123_init();
    mh = mpg123_new(NULL, &err);
    buffer_size = mpg123_outblock(mh);
    buffer = (unsigned char*) malloc(buffer_size * sizeof(unsigned char));
}

/*
* Opens the mp3 file
*/
void Player::openFile(){
    /* open the file and get the decoding format */
    mpg123_open(mh, "speaker/snelle.mp3");
    mpg123_getformat(mh, &rate, &channels, &encoding);
}

/*
* Sets the correct output format
*/
void Player::setOutputFormat(){
    /* set the output format and open the output device */
    format.bits = mpg123_encsize(encoding) * BITS;
    format.rate = rate;
    format.channels = channels;
    format.byte_format = AO_FMT_NATIVE;
    format.matrix = 0;
    dev = ao_open_live(driver, &format, NULL);
}

/*
* Plays the mp3 file that is opend
*/
void Player::playSound(){
    /* decode and play */
    if (!Player::isPlaying){
        Player::isPlaying = true;
    while (mpg123_read(mh, buffer, buffer_size, &done) == MPG123_OK)
        ao_play(dev, (char*)buffer, done);

    /* clean up */
    free(buffer);
    ao_close(dev);
    mpg123_close(mh);
    mpg123_delete(mh);
    mpg123_exit();
    ao_shutdown();
    Player::isPlaying = false;
    }
}

/*
* This method starts the program
*/
void *Player::start(void* atr){
    initPlayer();
    openFile();
    setOutputFormat();
    playSound();
}