/***
 * @author Aditya Sethi
 * Face recognition header file
 **/

/***
 * The imports needed for opencv
 **/
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

using namespace cv;

#ifndef FACERECOGNITION_H
#define FACERECOGNITION_H

class FaceRecognition
{
    // Private method
    private:
    // Public method
    public:
        static void detectAndDisplay(Mat frame, CascadeClassifier face_cascade);
        static void *startFaceRecognition(void *);
};


#endif