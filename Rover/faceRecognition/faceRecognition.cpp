/***
 * @author Aditya Sethi
 * Recognises faces in the camera stream
 * 
 * Sources:
 * https://docs.opencv.org/4.1.2/db/d28/tutorial_cascade_classifier.html
 **/

#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <unistd.h>

#include "faceRecognition.h"
#include "../communication/SocketServer.h"
#include "../settings.h"  /* IMPORT DEFAULT SETTINGS (OWN_IP & STREAM_PORT) */

#include <iostream>
#include <stdio.h>

using namespace std;
// Namespace for OpenCV
using namespace cv; 

#define DEBUG_MODE 1

#if DEBUG_MODE == 1
#define OUT(x) cout << "openCV: " << x << endl;
#else
#define OUT(x)
#endif


void *FaceRecognition::startFaceRecognition(void *){

    //Making cascade classifier object for the face
    CascadeClassifier face_cascade;

    /***
     * Adding haar cascade file to the cascade classifier
     * This are needed to detect faces
     **/
    string face_cascade_name = "faceRecognition/haarcascade_frontalface_alt.xml";

    //Load the cascade for the face
    if( !face_cascade.load( face_cascade_name ) )
    {
        cout << "Error loading face cascade\n";
    };

    // Making video capture opbject
    VideoCapture capture;
    
    usleep(3000000);

    // Adding the stream to this object
    if(capture.open(STREAM_IP)){
        cout << "face recognition connected with stream" <<endl;
    }
    else{
        cout << "Failed to connect to stream" << endl;
    }

    // Creating frame object
    Mat frame;
    cout << "read frame" << endl;

    int counter = 0;
    // While frames are available through the video capture, the franes will be analyzed
    while(capture.read(frame)) 
    {    
        // When the frame is empty, stop the video capture     
        if( frame.empty() )
        {
            cout << "No captured frame\n";
            break;
        }

        counter++;

        // Apply the face classifier to every 5th frame        
        if(counter % 5 == 0){
            detectAndDisplay(frame, face_cascade);
        }
    }

    // Release the capture object
    capture.release();
}

void FaceRecognition::detectAndDisplay(Mat frame, CascadeClassifier face_cascade)
{ 
    // Create a new frame to copy the original frame into
    Mat frame_gray; 
    // Convert the image to greyscale
    cvtColor(frame, frame_gray, COLOR_BGR2GRAY ); 
    // Normalize the brightness and increase contrast of the greyscale image
    equalizeHist(frame_gray, frame_gray ); 
    
    // Create a vector of rectanges which will contain the faces
    std::vector<Rect> faces; 

    /**
     * Detect objects in the image according to the face cascade classifier
     * The detected faces will be returned as a list of rectangles
     **/
    face_cascade.detectMultiScale(frame_gray, faces);

    // Draw circles around the detected faces
    for (size_t i = 0; i < faces.size(); i++ )
    {
        // Calculate the center of the faces
        Point center(faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
        // Draw a circle around the faces
        ellipse( frame, center, Size( faces[i].width/2, faces[i].height/2 ), 0, 0, 360, Scalar( 255, 0, 255 ), 4 );
        //Mat faceROI = frame_gray( faces[i] );
        
        // Save frame
        OUT("Found face");
    }

    if(faces.size() > 0){
        // Create message to send
        char msg[6];
        // One person found
        int foundPeople = 1;
        // Create a correct message with the code for face recognition
        SocketServer::toStateMsg(foundPeople, 9, msg);
        // Send message to client
        SocketServer::sendToClient(msg);
    }
    else{
        // Create message to send
        char msg[6];
        // One person found
        int foundPeople = 0;
        // Create a correct message with the code for face recognition
        SocketServer::toStateMsg(foundPeople, 9, msg);
        // Send message to client
        SocketServer::sendToClient(msg);
    }
    
}
