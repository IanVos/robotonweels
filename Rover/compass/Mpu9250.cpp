/***
* author: Ian Vos
* This code works with the mpu9250 sensor.
* With this code you can read data from the sensor, this includes: compass and themprature
***/

#include <wiringPiI2C.h>    /* Used to communicate over I2C */
#include <wiringPi.h>       /* Used to startup wiringpi */

#include <unistd.h>         /* Include sleep functions */
#include <stdlib.h>         /* Inlcude general functions */
#include <iostream>         /* Include c++ standard input/output functions */
#include <math.h>           /* Include mathmatical functions */

using namespace std;

#include "Mpu9250_reg.h" /* Inlude the defined registerlocations from the reg.h file*/

#include "Mpu9250.h"

/* Add debug options. use OUT(your text) to print only when debug mode is enabled */
#define DEBUG_MODE 0          /* Show debug output if it is 1*/
#if DEBUG_MODE == 1
#define OUT(x) cout << "Mpu9250: " << x << endl
#else
#define OUT(x)
#endif

int MPU::fd;
int MPU::comp;
int MPU::lastDirection;

/***
 * Start the MPU and enable all devices
 * WARNING: Needs to be run to enable Magnetometer I2C address
***/
void MPU::mpuInit(){
  OUT("Starting MPU");
  fd = wiringPiI2CSetup(SENS_ADDR);               /* Start the device and save it */
	wiringPiI2CWriteReg8 (fd, PWR_MGMT_1, 0x00);	  /* Wake the device */
  usleep(1000); /* Wait for reset*/
  wiringPiI2CWriteReg8 (fd, PWR_MGMT_1, 0x01);	  /* Set clock */
  usleep(1000); /* Wait for reset*/
	wiringPiI2CWriteReg8 (fd, CONFIG, 0x03);		    /* Write to Configuration register */
  usleep(1000); /* Wait for reset*/
  wiringPiI2CWriteReg8 (fd, SMPLRT_DIV, 0x00);	  /* Write to sample rate register */
  usleep(1000); /* Wait for reset*/
	wiringPiI2CWriteReg8 (fd, GYRO_CONFIG, 24);	    /* Write to Gyro Configuration register */
  usleep(1000); /* Wait for reset*/
	wiringPiI2CWriteReg8 (fd, INT_ENABLE, 0x0e);	  /*Write to interrupt enable register */
  usleep(1000); /* Wait for reset*/
  wiringPiI2CWriteReg8 (fd, INT_PIN_CFG, 0x0e);
  usleep(1000); /* Wait for reset*/
  comp = wiringPiI2CSetup(COMP_ADDR);             /* Start Magnetomter device and save it */
  usleep(10000);
  wiringPiI2CWriteReg8 (comp, 0x0a, 0x0);         /* Disable mesurements */
  sleep(1);
  wiringPiI2CWriteReg8 (comp, 0x0a, 0x16);         /* Start continuouse mesurement mode 8Hz (set to 0x6 for 100Hz)*/
  OUT("MPU init completed");
}

/***
 * Read the values from the given address and return as one address
 * @param Address(int): The register address the data is located
 * @param Dev(int): The device object, Comppas or MPU
 * @return (short): The complete value
 ***/
short MPU::readData(int address, int dev) {
  unsigned short lowByte, highByte;
  short int value;
  highByte = wiringPiI2CReadReg8(dev, address);     /* Read the upper part of the value */
	lowByte = wiringPiI2CReadReg8(dev, address+1);    /* Read the lower part of the value */
  value = (highByte << 8) | lowByte;                /* Stitch the values together */
  return value;
}

/***
 * Read the themprature from the sensor
 * @return(double): The themrature in degrees Celcius
 ***/
double MPU::readThemprature(){
  return (readData(THEMP_OUT, fd))/340 + 36.53;
}

/***
 * Check the data ready register,
 * @return(bool): True if data is ready false in other cases
 ***/
bool MPU::checkDataReady(){
  int tmp = wiringPiI2CReadReg8(comp, COMP_ST); // Read the compass status
  if(tmp && 0x01){
    return true;
  } else {
    return false;
  }
}

/***
 * Wait until the data is ready
 * If not ready wait one second
 ***/
void MPU::waitDataReady(){
  bool readyStatus =  checkDataReady();
  if(readyStatus){
    return;
  } else{
    usleep(100);
    waitDataReady();
  }
}

/***
 * Read the values from the compass and calculates the orientation angle 
 * @return(double): The orientation angle of the rover
 ***/
double MPU::getCompassAngle(){
  double angle;
  short x,y,z;

  /* Wait with mesurment on data ready */
  waitDataReady();
  
  /* Get measurments from the compass */
  usleep(100);
  x = readData(COMP_XOUT, comp);
  usleep(100);
  y = readData(COMP_YOUT, comp);
  usleep(100);
  z = readData(COMP_ZOUT, comp);
  usleep(100);
  wiringPiI2CReadReg8(comp, COMP_ST2); /* Read to confirm the reading is ready */
  usleep(100);
  OUT("X: " << x);
  OUT("Y: " << y);
  OUT("Z: " << z);
  /* Confert the x and y values to the heading of the car */
  angle = atan2((double)y, (double)x) * 180 / M_PI;
  if(angle < 0) angle +=360.0;

  /* Save angle for quick access */
  MPU::lastDirection = (int)angle;

  return angle;
}

/***
 * Get the last mesured direction
 ***/
int MPU::getLastDirection(){
  return MPU::lastDirection;
}

/***
 * Set the last mesured value
 ***/
void MPU::setLastDirection(int direction){
  MPU::lastDirection = direction;
}


