/***
 * @author Ian Vos
 * Send the movements of the rover to a server to map the route it took.
***/

#define SEND_PREFIX 3

#define DEBUG_MODE 1
#if DEBUG_MODE == 1
#define OUT(x) cout << "RouteMapping: " << x << endl
#else
#define OUT(x)
#endif

#define TO_SEC(x) x * 1000

#include <iostream>   
#include <unistd.h>   /* sleep */
#include <stdlib.h>   /* rand */
#include <time.h>     /* time */
#include <chrono>
#include <string.h>
#include <string>
#include <sstream>

#include "Mpu9250.h"
#include "../communication/SocketServer.h"
#include "../communication/SocketClient.h"
#include "../sensors/speed.h"

#include "RouteMapping.h"

using namespace std;

int RouteMapping::routeId = 0;

/***
 * Create a new random route id and send to server
 ***/
void RouteMapping::createRouteId(){
  OUT("Create a new route id");
  srand(time(NULL));    /* Set a value as base for rand() */
  RouteMapping::routeId = rand() % 1000000;

  /* Send the route ID */
  char id[16];
  strcpy(id, to_string(RouteMapping::routeId).c_str()); 

  string RouteIdString = to_string(routeId) + '\n';
	strcpy(id, RouteIdString.c_str());
  
  SocketClient::sendToServer(id);
  OUT("New route ID = " << RouteMapping::routeId);
}

/***
 * Calculate the distance travelled in meters from the speed and time 
 * @param speed(int): The speed of the rover over a given time (km/h)
 * @param time(double): The time of measurement of the speed
 * @return (int) distance in meters
 ***/
int RouteMapping::calculateDistance(int speed, double time){
  /* convert km/h to m/s and multiply by the time in seconds to get distance in meters */
  return (speed / 3.6) * time;
}


/***
 * Compass mapping thread
 * Send the direction to the app and the map vector to the server
 * @param atr (void*) a atribute the thread can send
***/
void* RouteMapping::compassMapping(void* atr){
  OUT("Start mapping thread");

  /* Start the MPU */
  MPU::mpuInit();

  /* Connect to mapping server */
  SocketClient::startConnection();

  /* Create a new route id to identify the route */
  RouteMapping::createRouteId();

  int averageSpeed = 0, averageDirection = 0;
  double elapsed_time_ms;

  while(true){
    auto t_start = chrono::high_resolution_clock::now();  /* Get the timer start time */
    OUT("Clock started");

    /* Send previous calculation */
    char msg[16];
    int distance = RouteMapping::calculateDistance(averageSpeed, TO_SEC(elapsed_time_ms));
    SocketClient::convertToChar(distance, averageDirection, msg); /* Create a char array that can be send in msg */
    SocketClient::sendToServer(msg);  /* Send the char array msg to the server */

    /* Declare totals to mesure the avarge*/
    int totalSpeed = 0;
    double totalDirection = 0;

    /* Reset the average values */
    averageSpeed = 0;
    averageDirection = 0;

    /* Measure ten times with an interval of 0.1 second */
    for(int i = 0; i < 10; i++ ){
      totalSpeed = totalSpeed + Speedsensor::getLastSpeed();      /* Add last mesured speed value to the total of ten */
      totalDirection = totalDirection + MPU::getCompassAngle();   /* Mesure a new angle and add to total value */
      usleep(100000);
    }

    averageSpeed = totalSpeed / 10;
    averageDirection = totalDirection / 10;
    sendCompass(averageDirection);

    auto t_end = chrono::high_resolution_clock::now();    /* Get the timer end time */
    elapsed_time_ms = chrono::duration<double>(t_end-t_start).count(); /* Calculate elapsed time in seconds*/
  }
}

/***
 * Send the value of the compass to the app
 * @param direction: the direction the rover is facing
 ***/
void RouteMapping::sendCompass(int direction){
  OUT("SEND " << direction);
  if (direction != MPU::getLastDirection()){
    char msg [6];
    SocketServer::toStateMsg(direction, SEND_PREFIX, msg);
    SocketServer::sendToClient(msg);
    OUT("SENDED: " << direction);
  }
  MPU::setLastDirection(direction);
}