/***
 * @author Ian Vos
 * Send the movements of the rover to a server to map the route it took.
 ***/
 
/* Route vector struct */
struct routeVector{
  int direction;
  int distance;
};

 class RouteMapping {
   private:
    static int routeId;
    static void createRouteId();
    static void sendCompass(int direction);
    static int calculateDistance(int speed, double time);
    
   public:
    static void* compassMapping(void* atr); /* Thread function */
 };
