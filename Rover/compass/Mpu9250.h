/***
 * @author Ian Vos 
 * 
 ***/

#ifndef MPU_H
#define MPU_h

class MPU 
{
private:
  static int lastDirection, fd, comp;
  static short readData(const int address, const int dev);
  static bool checkDataReady();

public:
  static void mpuInit();
  static void waitDataReady();  
  static double readThemprature();
  static double getCompassAngle();
  static int getLastDirection();
  static void setLastDirection(int direction);
};

#endif