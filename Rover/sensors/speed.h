/***
 * @author: Jeffrey Faas
 * Speed header file
 *
 ***/

#ifndef SPEED_H
#define SPEED_H

class Speedsensor
{
    private :
        static int counter, lastSpeed;
    public :
        static void do_count();
        
        void getTimerIsr();
        void setup();
        void *speed(void);
        static int getLastSpeed();
        static void *speed_helper(void *context);
};

#endif