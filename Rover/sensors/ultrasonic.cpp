/***
 * @author Jeffrey Faas
 * The ultrasonic sensor of the rover, here you find the class with methods to move the servo in a mapped area and
 * a methods to calculate the distance between an object and the robotcar in centimeters
 ***/

#include  <stdio.h>
#include  <unistd.h>
#include  <wiringPi.h>
#include  <wiringPiI2C.h>
#include  <softPwm.h>
#include  <pthread.h>
#include  "ultrasonic.h"
#include  <iostream>

/* Local header files */
#include "../communication/SocketServer.h"
#include "../movement/movement.h"

using namespace std;

/***
* Setup for Pin 1 and PWM
* Called in ultrasonic
***/
void Ultrasonic::setup(){

/***
 * Set wiringpin pin 1 to output
 ***/
//PIN:
pinMode(1,OUTPUT);
digitalWrite(1,LOW);

/***
 * Set the pwmclock and setup the pwm range and pin
 ***/
//PWM settings:
pwmSetClock(500);                           //add explanation
softPwmCreate(1,0,500);                     //add explanation
usleep(1000);                               //delay
}

/***
* Function calculates the distance in centimeters
* Print distance in cm
* Called in ultrasonic
***/
void Ultrasonic::getDistance()
{
    //Create class object
    SocketServer server;

    int fd2;
    int checkValue;
    int ultrasonicValue;
    char msg [6];    

    //ULTRASONIC setting:
    fd2=wiringPiI2CSetup(0x70);
    uint8_t TxData1[3] = {00,0x51};         //add second array element; 
    uint8_t TxData2[1] = {0x03};            //What is the function of memory location 3?
    uint8_t low=TxData1[3];

    //Read and write distance:
    write(fd2,&TxData1[0],2);
    usleep(100000);                         //give the sensor time for measurement
    write(fd2,&TxData2[0],1);               //ask for the lower order byte of the range
    usleep(100000);
    read(fd2,&low,1);

    ultrasonicValue = low;

    /***
    *If statement checks if the value has been changed
    *If the value has checked it gives the value a state and sends it to the client
    ***/
    if (ultrasonicValue != checkValue){
        server.toStateMsg(ultrasonicValue, 2, msg);
        server.sendToClient(msg);
        if(ultrasonicValue < 30) {
          Movement movement;
          movement.stop();
        } 
    }
    checkValue = ultrasonicValue;
}

/***
* Function moves the servo within a map and the function prints the distance in cm
* Function calls setup() and distance()
***/
void *Ultrasonic::ultrasonic(void)          //function to move servo within an area
{
    setup();
    int position = 10;  /* Default start position */
    int i = 10;         
    while(1){
      softPwmWrite(1, position);
      if (i == 30){ //set index to beginning value
        i = 10;
      }
      else if (i >= 20){                      //servo move left
        position = position - 1;
        i++;
      }
      else {                                  //servo move right
        position++;
        i++;
      }
      getDistance();
      usleep(1000);
    }
}
/***
* Method helps to find the function ultrasonic
***/
void *Ultrasonic::ultrasonic_helper(void *context)
{
    return ((Ultrasonic *)context)->ultrasonic();
}