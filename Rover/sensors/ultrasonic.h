/***
 * @author: Jeffrey Faas
 * Ultrasonic header file
 *
 ***/
 
#ifndef ULTRASONIC_H
#define ULTRASONIC_H

class Ultrasonic
{ 
  private:
    typedef unsigned char uint8_t;
  public:
    void setup();
    void getDistance();
    void *ultrasonic(void);
    static void *ultrasonic_helper(void *context);
};

#endif