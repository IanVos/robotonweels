/***
 * @author Jeffrey Faas
 * The speed sensor of the rover, here you find the class with methods to calculate the speed in kilometers per hour
 ***/

#include  <stdio.h>
#include  <unistd.h>
#include  <wiringPi.h>
#include  <wiringPiI2C.h>
#include  <softPwm.h>
#include  <pthread.h>
#include  "speed.h"
#include  <math.h>
#include  "../communication/SocketServer.h"
#include <iostream>

using namespace std;

int Speedsensor::counter;
int Speedsensor::lastSpeed = 0;
/***
* Every interrupt add 1 to counter
* Called in function setup()
* Global counter(int):add 1 every time
***/
void Speedsensor::do_count() {
  counter++;
}

/***
* Calculates the speed of the rover
* Print speed in km/h
***/
void Speedsensor::getTimerIsr() {
  //Create class object
  SocketServer server;

  int speedValue;
  char msg [6];

  double diameterWheel = 0.0550;                        //in meters
  double actual = (counter / 20);                         //one actual is 1 full turn of the wheel
  double speed = (diameterWheel * actual * 3.6);        //formula to calculate the speed in km/h
  speedValue = floor(speed);

  /***
  *If statement checks if the value has been changed
  *If the value has checked it gives the value a state and sends it to the client
  ***/

  usleep(500000);

  if (speedValue != Speedsensor::lastSpeed){
    printf("Motor speed is %.0f Km/h\n", speed);          //print speed in km/h
    server.toStateMsg(speedValue, 1, msg);
    cout<<msg<<endl;
    server.sendToClient(msg);
  }
  Speedsensor::lastSpeed = speedValue;  /* Save the latest speed mesurement */
  counter= 0;                                           //set counter to 0
}

/***
* Setup for wiringPiISR and resistor an Pin 2
* wiringPiISR detects interrupts
* pullUpDnControl adds resistor to pin 2
***/
void Speedsensor::setup(){
  pinMode(2, INPUT);
  wiringPiISR (2, INT_EDGE_FALLING, NULL);
  wiringPiISR(2, INT_EDGE_SETUP, do_count);               //when an interrupt happens this function calls
  pullUpDnControl(2,PUD_UP);                              //add resistor to pin 2
}

/***
* Runs setup() once
* Every second the speed is calculuated in timeIsr()
***/
void *Speedsensor::speed(void){
  setup();                                                //setup

  while(1){
    usleep(500000);                                      //every second the speed is calculated
    getTimerIsr();
  }
}
/***
* Helps to find the function speed
***/
void *Speedsensor::speed_helper(void *context)            //method to find the function speed
{
  return ((Speedsensor *)context)->speed();
}

int Speedsensor::getLastSpeed(){
  return Speedsensor::lastSpeed;
}
