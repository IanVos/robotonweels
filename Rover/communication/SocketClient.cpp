/***
 * @author Ian Vos
 * Socket client that connects to a server
 * Connect to the webserver for the route mapping and send the route data
 ***/

/* Include libs */
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <sstream>
#include <cmath>
#include <math.h>

#include "SocketClient.h"

using namespace std;

#define PORT 10001
#define HOST_IP "192.168.1.14"

#define DEBUG_MODE 0
#if DEBUG_MODE == 1
#define OUT(x) cout << "SocketClient: " << x << endl
#else
#define OUT(x)
#endif

int SocketClient::sock = 0;
bool SocketClient::isConnected = false;

/***
 * Start connection with the server
 *
 ***/
void SocketClient::startConnection(){
  OUT("START Con");
	struct sockaddr_in serv_addr; 
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
	{ 
		OUT("Socket creation error");
    SocketClient::isConnected = false;
		return; 
	} 

	serv_addr.sin_family = AF_INET; 
	serv_addr.sin_port = htons(PORT); 
	// Convert IPv4 and IPv6 addresses from text to binary form 
	if(inet_pton(AF_INET, HOST_IP, &serv_addr.sin_addr)<=0) 
	{ 
    OUT("Invalid address/ Address not supported"); 
    SocketClient::isConnected = false;
		return; 
	} 
	if (connect(SocketClient::sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
	{ 
		OUT("Connection failed");
    SocketClient::isConnected = false;
		return; 
	}
  SocketClient::isConnected = true;
  OUT("Connected");
}

/***
 * Send a message to the server
 *
 ***/
void SocketClient::sendToServer(char *msg){
  if(!SocketClient::isConnected){
    SocketClient::startConnection();
  }
  if(SocketClient::isConnected){
    send(SocketClient::sock , msg , strlen(msg) , 0 );
    OUT("Send: " << msg);
  }
}

void SocketClient::convertToChar(int value1, int value2, char* output){
  char msg[16];
	string stringMsg = to_string(value1) + '_'+ to_string(value2) + '\n';
	strcpy(msg, stringMsg.c_str());
  /* Copy string to out value */
	for(int i = 0; i < 16; i++){
		output[i] = msg[i];
	}
}