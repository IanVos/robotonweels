/***
 * @author: Ian Vos & Jeffrey Faas
 * Socket server header file
 *
 ***/

#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

class SocketServer{
  private:
    static int clientSocket;
    static const int PORT;
    static const int BUFFER_SIZE;
    static int listening;
    static bool clientConnected;
  public:
    static void startServer();

    static void acceptClient();
    static int listenToClient();

    static void closeSocket();

    static void sendToClient(char msg[4]);
    static void toStateMsg(int value, int state, char* out);
    //static void sendImage(auto data, int total);
};

#endif