/***
 * @author Ian Vos
 * Socket client that connects to a server
 * Connect to the webserver for the route mapping and send the route data
 ***/

#ifndef SOCKETCLIENT_H
#define SOCKETCLIENT_H

 class SocketClient{
   private:
    static int sock;
    static bool isConnected;
   public:
    static void startConnection();
    static void sendToServer(char *msg);
    static void convertToChar(int value1, int value2, char* output);
 };

 #endif