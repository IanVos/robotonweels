/***
 * @author: Ian Vos & Jeffrey Faas
 * Socket server basics
 *
***/

#define DEBUG_MODE 1
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <sstream>
#include <cmath>
#include <math.h>

using namespace std;

#include "SocketServer.h"

#if DEBUG_MODE == 1
  #define OUT(x) cout << "SocketServer: " << x << endl
#else
  #define OUT(x)
#endif

/***
* Global variables
***/ 
const int SocketServer::PORT = 1337;
const int SocketServer::BUFFER_SIZE = 4096;
int SocketServer::clientSocket = 6;
int SocketServer::listening = 0;
bool SocketServer::clientConnected = false;


/***
	* Create a new socket
	* @return: the new socket object ???
	***/
void SocketServer::startServer(){

	listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == -1)
	{
		cerr << "Can't create a socket! Quitting" << endl;
	}
	
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(SocketServer::PORT);
	inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr);

	bind(listening, (sockaddr*)&hint, sizeof(hint));

	cout << "Server started";
}

/***
* This method listens until it find a client
* If the client has been found it connects to it
***/ 
void SocketServer::acceptClient(){
  	listen(listening, SOMAXCONN);

	// Wait for a connection
	sockaddr_in client;
	socklen_t clientSize = sizeof(client);

	int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);

	char host[NI_MAXHOST];      		// Client's remote name
	char service[NI_MAXSERV];   		// Service (i.e. port) the client is connect on

	memset(host, 0, NI_MAXHOST); 		// same as memset(host, 0, NI_MAXHOST);
	memset(service, 0, NI_MAXSERV);

	// Connecting to port
	if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		cout << host << " connected on port " << service << endl;
	}
	else
	{
		inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
		cout << host << " connected on port " << ntohs(client.sin_port) << endl;
	}
  SocketServer::clientConnected = true;
}

/***
* In this method the server waits until it receives a message from the server
* It also tells if the client has been disconnected
***/ 
int SocketServer::listenToClient(){
	char buf[SocketServer::BUFFER_SIZE];

  /* Listen to a message */
	while (true)
	{
		memset(buf, 0, SocketServer::BUFFER_SIZE);

		// Wait for client to send data
		int bytesReceived = recv(clientSocket, buf, SocketServer::BUFFER_SIZE, 0);
		if (bytesReceived == -1)
		{
			cerr << "Error in recv(). Quitting" << endl;
      SocketServer::clientConnected = false;
			return 999;
		}

		if (bytesReceived == 0)
		{
			cerr << "Client disconnected " << endl;
      SocketServer::clientConnected = false;
			return 999;
		}
		
		// Convert the incomming bytes to a string msg
    	OUT("Test1");
		string receivedString = string(buf, 0, bytesReceived);
    	OUT(receivedString);
		
		// Convert the incomming msg to an int
		int received = std::stoi(receivedString);
    	return received;
	}
  
  return 999;
}

/***
 *  Close the socket
 ***/
void SocketServer::closeSocket(){
  OUT("closing the socket");
  SocketServer::clientConnected = false;
	close(clientSocket);
  OUT("SocketServer: Socket closed");
}

/***
* Method to send data to the client
***/ 
void SocketServer::sendToClient(char msg[6]){
  if(SocketServer::clientConnected){
  	send(clientSocket, msg, 6, 0);
  }
}

/***
* Adds a state to the sensor values
***/ 
void SocketServer::toStateMsg(int value, int state, char* out){

	char msg[6];
	string stringMsg = to_string(state) + to_string(value) + '\n';
	strcpy(msg, stringMsg.c_str());

	for(int i = 0; i < 6; i++){
		out[i] = msg[i];
	}
	
}

/***
 * Send a image to the client
 * @param data: the data of the frame to be send
 * @param total: the total size of the data to be send
 ***/
// void SocketServer::sendImage(auto data, int total){
//   send(clientSocket, data, total, 0);
// }