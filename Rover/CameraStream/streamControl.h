/**
 * @author Aditya Sethi
 * StreamControl Header file
 **/

class StreamControl{
	public:	
		void streamStart();
		void streamStop(); 
};
