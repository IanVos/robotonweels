#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "streamControl.h"

/**
 * Starts the motion stream 
 * Uses the streamStart.sh shell script
 * */
void StreamControl::streamStart(){
	system("CameraStream/streamStart.sh");
}

/**
 * Stops the motion stream 
 * Uses the streamStop.sh shell script
*/
void StreamControl::streamStop(){
	system("CameraStream/streamStop.sh");
}
