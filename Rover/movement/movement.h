/***
 * @author: Ian Vos, Jeffrey Faas & Aditya Sethi
 * Movement header file
 *
 ***/

#ifndef MOVEMENT_H
#define MOVEMENT_H

class Movement{
  public:
    typedef unsigned char uint8_t; // Unsigned chars can be declared as uint8_t. New variables can easily be made

    static int fd;

    static const uint8_t MotorForward[];  // High speed forward
    static const uint8_t MotorStop[];           // Stop left + right
    static const uint8_t MotorBackward[]; // High speed backward left + right
    static const uint8_t MotorLeft[];     // High speed forward left and backward right
    static const uint8_t MotorRight[];    // High speed forward right and 
    
    /**
    * Declaring all the needed functions and variables
    */
    static void Motorinit(); // Function to initialize the motors
    static void moveTo(int dir); // Function to move the car in certain directions

    /**
    * The movement functions
    */
    static void left();
    static void right();
    static void forward();
    static void backward();
    static void stop();

};

#endif