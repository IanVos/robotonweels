/***
 * @author: Ian Vos, Aditya Sethi & Jeffrey Faas
 * Control the movement of the rover
 ***/

#include <stdio.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <pthread.h>

#include "movement.h"

typedef unsigned char uint8_t;

int Movement::fd;
const uint8_t Movement::MotorForward[7] = {7,3,0xa5,2,3,0xa5,2};  // High speed forward
const uint8_t Movement::MotorStop[7] = {7,0,0,0,0,0,0};           // Stop left + right
const uint8_t Movement::MotorBackward[7] = {7,3,0xa5,1,3,0xa5,1}; // High speed backward left + right
const uint8_t Movement::MotorLeft[7] = {7,3,0xa5,2,3,0xa5,1};     // High speed forward left and backward right
const uint8_t Movement::MotorRight[7] = {7,3,0xa5,1,3,0xa5,2};    // High speed forward right and 

void Movement::Motorinit() {
	/**
	 * The total power that the motors can use
	 */
    uint8_t Totalpower[2]={4,255};    // First value is always 4. Second value the power, and is between 0 and 255
	
	/**
	 * Soft start is used to delay the speed at which the power is given to the motors. 
	 * The value 20 would mean that it takes 2ms * power to reach the power.
	 * In this case, all power is applied immediatly, because the value is 0	
	 **/
    uint8_t Softstart[3]={0x91,23,0};  // Writes the value 0 to location 23, the location of soft start


    wiringPiSetup () ; // Initializing wiringPi
    pullUpDnControl(0,PUD_DOWN); // Set the pull-up or pull-down resistor mode off on pin 0

    fd = wiringPiI2CSetup(0x32); // Initializing fd, which is needed to check which version of raspberry pi is being used

    write(fd,&Totalpower[0], 2);  // Writes the two bytes that are in Totalpower[] using the fd variable 
    write(fd,&Softstart[0],3);  // Writes the three bytes that are in Softstart[] using the fd variable
}

/**
 * Simple method to move the car
 * @param dir: an integer between -1 and 3. Will be used for the direction in which the car needs to move
 */
void Movement::moveTo(int dir) {
    switch(dir)
    {
        case -1: // stop
            stop();
            break;
        case 0: // forward
            forward();
            break;
        case 1: // backward
            backward();
            break;
        case 2: // left
            left();
            break;
        case 3: // right
            right();
            break;
            
    }
}

/***
 * To move the car, 7 needs to be the value for the first index of the array with corresponding direction
 * These are five methods that do this for every movement
 ***/
void Movement::left() {
  write(fd,&Movement::MotorLeft[0],7); 
}
void Movement::right() {
  write(fd,&Movement::MotorRight[0],7); 
}
void Movement::forward() {
  write(fd,&Movement::MotorForward[0],7);
}
void Movement::backward() {
  write(fd,&Movement::MotorBackward[0],7);
}
void Movement::stop() {
  write(fd,&Movement::MotorStop[0],7);
  usleep(500000);  // With this sleep the car will stop without immediately moving again
}

